# Image steganography using LSB

## Installation
Dependencies: 
* [pipenv](https://pipenv.readthedocs.io/en/latest/)
* Python 3.7 or [pyenv](https://github.com/pyenv/pyenv)

Execute:
```bash
git clone https://gitlab.com/maciejsudol/steg
pipenv install
```

## Usage
```bash
pipenv run python steg.py encode --img_path <img_path> --in_data <in_data> --out_data <out_data>
pipenv run python steg.py decode --img_path <img_path> --out_data <out_data>
```

where:
* `img_path`: Path to image file (`.png`, `.bmp` or other bitmap format)
* `in_data`: Path to input data file
* `out_data`: Path to output data file