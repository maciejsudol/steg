# coding=utf-8
"""
"""

import os


def open_file(path: str):
    try:
        os.startfile(path)
    except FileNotFoundError:
        os.startfile(os.path.join(os.getcwd(), path))