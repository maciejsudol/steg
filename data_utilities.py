from bitstring import BitArray


class DataUtilities:
    class DataSplitter:
        BUFFER = '0'

        def __init__(self, path: str):
            self.data_path = path
            self.data_type = path.split('.')[-1]

            with open(self.data_path, 'rb') as input_data:
                self.bin_data = BitArray(bytes=input_data.read())

        def __iter__(self):
            for bit in self.bin_data.bin:
                yield bit
            yield self.BUFFER

    class DataBuilder:
        def __init__(self, path):
            self.data_path = path
            self.data_type = path.split('.')[-1]

            self.builder = BitArray()

        def append_bit(self, bit: int):
            self.builder.append(bin(bit))

        def save_to_file(self):
            with open(self.data_path, 'wb') as output_data:
                self.builder.tofile(output_data)

    @staticmethod
    def set_bit(byte: int, position: int, value: int) -> int:
        new_byte = list(format(byte, '#010b'))[2:]
        new_byte[-position - 1] = value

        return int(str.join('', new_byte), 2)

    @staticmethod
    def get_bit(byte: int, position: int) -> int:
        return int(list(format(byte, '#010b'))[2:][-position - 1])

    @staticmethod
    def get_data_splitter(input_path):
        return DataUtilities.DataSplitter(input_path)

    @staticmethod
    def get_data_builder(output_path):
        return DataUtilities.DataBuilder(output_path)


if __name__ == '__main__':
    data_splitter = DataUtilities.get_data_splitter('data/message/lorem.txt')

    for x in data_splitter:
        print(x)
