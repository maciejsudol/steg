# coding=utf-8
"""
Simple steganography demo.

Usage:
    steg.py encode --img_path <img_path> --in_data <in_data> --out_data <out_data>
    steg.py decode --img_path <img_path> --out_data <out_data>

Arguments:
    img_path        Path to image file
    in_data         Path to input data file
    out_data        Path to output data file

Options:
    -h --help               Display this help and exit
    --version               Show version
"""
import collections

from PIL import Image, ImageOps, ImageChops
from docopt import docopt

from data_utilities import DataUtilities
from utilities import open_file


class StegImg:
    """
    Provides functionalities to manipulate steganography images
    """
    NUMBER_OF_BITS_TO_CHANGE = 2

    def __init__(self, path):
        """
        Initializes StegImg instance
        :param path: path to image
        """

        self.data_utilities = DataUtilities()

        self.img_path = path
        self.img = Image.open(path)

        self.steg_img = None
        self.output_path = None
        self.size_x, self.size_y = self.img.size
        self.img_mode = self.img.mode
        self.img_type = path.split('.')[-1]

    def __repr__(self):
        return ''

    def _save_img(self, img, path):
        self.steg_img = img
        self.output_path = path

        img.save('%s.%s' % (path, self.img_type), self.img_type)

    def _save_file(self, data_builder, path):
        self.output_path = path

        data_builder.save_to_file()

    def _img_iterator(self):
        for x in range(self.size_x):
            for y in range(self.size_y):
                yield ((x, y), self.img.getpixel((x, y)))

    def _create_img_diff(self, target_img: Image, output_path: str):
        for (source_channel, target_channel, channel) in [(self.img.getchannel(band), target_img.getchannel(band), band) for band in self.img.getbands()]:
            diff = ImageOps.autocontrast(ImageChops.difference(source_channel, target_channel))
            self._save_img(diff, f"{output_path}-{channel}-channel-diff")

    def encode_message(self, input_path: str, output_path: str):
        """
        Encodes given data into image
        :param input_path:  data input path
        :param output_path: image output path
        """
        steg_img = self.img.copy()
        data_iterator = iter(self.data_utilities.get_data_splitter(input_path))

        for (coordinates, pixel) in self._img_iterator():
            if isinstance(pixel, collections.abc.Iterable):
                new_pixel = pixel

                for lsb in range(self.NUMBER_OF_BITS_TO_CHANGE):
                    try:
                        new_pixel = tuple(
                            self.data_utilities.set_bit(channel, lsb, next(data_iterator)) for channel in new_pixel)

                        if len(new_pixel) != len(pixel):
                            raise StopIteration
                    except StopIteration:
                        break
                    steg_img.putpixel(coordinates, new_pixel)
            else:
                new_pixel = pixel

                for lsb in range(self.NUMBER_OF_BITS_TO_CHANGE):
                    try:
                        new_pixel = DataUtilities.set_bit(new_pixel, lsb, next(data_iterator))
                    except StopIteration:
                        break
                steg_img.putpixel(coordinates, new_pixel)

        # diff = ImageOps.autocontrast(ImageChops.difference(self.img, steg_img))
        # self._save_img(diff, f"{output_path}-diff")
        self._create_img_diff(steg_img, output_path)
        self._save_img(StegImg.lsb(self.img), "in-lsb")
        self._save_img(StegImg.lsb(steg_img), f"{output_path}-lsb")
        self._save_img(steg_img, output_path)

    def decode_message(self, output_path: str):
        """
        Decodes message from given image data
        :param output_path: data output path
        """
        data_builder = self.data_utilities.get_data_builder(output_path)

        for (coordinates, pixel) in self._img_iterator():
            if isinstance(pixel, collections.abc.Iterable):
                for lsb in range(self.NUMBER_OF_BITS_TO_CHANGE):
                    for channel in pixel:
                        data_builder.append_bit(self.data_utilities.get_bit(channel, lsb))
            else:
                for lsb in range(self.NUMBER_OF_BITS_TO_CHANGE):
                    data_builder.append_bit(self.data_utilities.get_bit(pixel, lsb))

        self._save_file(data_builder, output_path)

    @staticmethod
    def lsb(image):
        lut = []
        mask = 2 ** 4 - 1
        for i in range(256):
            lut.append(i & mask)
        return ImageOps.autocontrast(ImageOps._lut(image.convert('L'), lut))

    def get_img_output_path(self):
        return f'{self.output_path}.{self.img_type}'

    def get_file_output_path(self):
        return self.output_path


if __name__ == '__main__':
    ARGS = docopt(__doc__, version='steg 0.1')

    if ARGS['encode']:
        img_path = ARGS['<img_path>']
        in_data_path = ARGS['<in_data>']
        out_data_path = ARGS['<out_data>']

        steg_img = StegImg(img_path)
        steg_img.encode_message(in_data_path, out_data_path)

        open_file(steg_img.get_img_output_path())

    if ARGS['decode']:
        img_path = ARGS['<img_path>']
        out_data_path = ARGS['<out_data>']

        steg_img = StegImg(img_path)
        steg_img.decode_message(out_data_path)

        open_file(steg_img.get_file_output_path())
